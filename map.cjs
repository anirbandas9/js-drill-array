const elements = require('./data.cjs');

// function cb(element) {
//     return element;
// }

function map(elements, cb) {
    const arr = [];

    if (!Array.isArray(elements) || map.arguments.length < 2 || cb === undefined) {
        return [];
    }

    for (let index = 0; index < elements.length; index++) {
        const eachElement = cb(elements[index], index, elements); 
        arr.push(eachElement);
    }
    return arr;
}

// let a = map(elements, (value) => value);

// console.log(a);

module.exports = map;


