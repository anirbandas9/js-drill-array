const elements = require('./data.cjs');

function reduce(elements, cb, startingValue = elements[0]) {

    if (!Array.isArray(elements) || reduce.arguments.length < 2) {
        return [];
    }

    if (reduce.arguments.length === 2) {
        for (let index = 1; index < elements.length; index++) {
            startingValue = cb(startingValue, elements[index], index, elements);
        }
    }
    else {
        for (let index = 0; index < elements.length; index++) {
            startingValue = cb(startingValue, elements[index], index, elements);
        }
    }
    return startingValue;
}

// let a = reduce(elements, (acc, value) => {
//     return acc + value;
// }, 0);

// console.log(a);

module.exports = reduce;
