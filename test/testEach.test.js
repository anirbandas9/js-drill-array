const data = require('../data.cjs');
const each = require('../each.cjs');


// let result = each(data, (value) => {
//     return value;
// });

function cb(element) {
    return element;
}


test('Testing Each function', () => {
    expect(each(data, cb)).toStrictEqual([1, 2, 3, 4, 5, 5]);
});
