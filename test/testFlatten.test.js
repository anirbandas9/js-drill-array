const data = require('../flattenData.cjs');
const flatten = require('../flatten.cjs');

// const data = [1, [2], [[3]], [[[4]]]];

test('Testing Flatten function', () => {
    expect(flatten(data, 2)).toStrictEqual(data.flat(2));
});

// console.log(data);