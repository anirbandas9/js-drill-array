const data = require('../data.cjs');
const reduce = require('../reduce.cjs');

function cb(acc, value, index, array) {
    return acc + value;
}

test('Testing Reduce function', () => {
    expect(reduce(data, cb, 0)).toStrictEqual(data.reduce((acc, value, index, array) => acc + value, 0));
});