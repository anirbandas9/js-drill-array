const data = require('../data.cjs');
const map = require('../map.cjs');

function cb(element, index, array) {
    return element;
}

// const result10 = map(data, parseInt);
// console.log(result10);


test('Testing Map function', () => {
    expect(map(data, cb)).toStrictEqual(data.map((value, index, array) => value));
    expect(map(data, parseInt)).toStrictEqual(data.map(parseInt));
});