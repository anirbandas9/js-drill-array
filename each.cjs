const elements = require('./data.cjs');

// function cb(element) {
//     return element;
// }

function each(elements, cb) {
    const result = [];

    if (!Array.isArray(elements) || each.arguments.length !== 2) {
        return [];
    }

    for (let index = 0; index < elements.length; index++) {
        result.push(cb(elements[index]));
        // cb(elements[index], index);
    }
    return result;
}

// let a = each(elements, (value) => value);

// console.log(a);

module.exports = each;