const elements = require('./data.cjs');

function find(elements, cb) {

    if (!Array.isArray(elements) || find.arguments.length !== 2) {
        return [];
    }
    
    for (let index = 0; index < elements.length; index++) {
        const result = cb(elements[index]);
        
        if (result === true) {
            return elements[index];
        }
    }
    return undefined;
}

// let a = find(elements, (value) => value > 4);

// console.log(a);

module.exports = find;